def calcular_energia_inicial():
    return 6


def calcular_energia_restante(energia_inicial, energia_quita):
    return energia_inicial - energia_quita


def golpe_especial(personaje, movimiento, golpe):
    tonyn = {"DSD + P": 3, "SD + K": 2, "D + K": 1,"DSD + K":1,'SD + P':1,'SDD + K':1,'SA + K':1}
    arnaldor = {"SA + K": 3, "ASA + P": 2, "P o K": 1, 'SA + P':1,'DSD + P':1,'WSAW + K':1,'SA + K':1,'ASA + K':1}

    if personaje == "Tonyn Stallone":
        if golpe != "" and movimiento != "":
            golpe_actual = movimiento + " + " + golpe
            return tonyn[golpe_actual]
        return 0
    elif personaje == "Arnaldor Shuatseneguer":
        if golpe != "" and movimiento != "":
            golpe_actual = movimiento + " + " + golpe
            return arnaldor[golpe_actual]
        return 0
    else:
        return 0


def determinar_ganador(turno, player1_energia, player2_energia):
    if turno % 2 == 1:
        return "Tonyn Stallone" if player1_energia > 0 else "Arnaldor Shuatseneguer"
    else:
        return "Arnaldor Shuatseneguer" if player2_energia > 0 else "Tonyn Stallone"
        
def player1(turno,movimiento_p1,golpe_p1,player2_energia):
    d = dict();
    energia_quita = golpe_especial("Tonyn Stallone", movimiento_p1, golpe_p1)
    #print('player 1 quita', energia_quita)
    d['player2_energia'] = calcular_energia_restante(player2_energia, energia_quita)
    d['mensaje'] = narrar_turno(turno, "Tonyn Stallone", movimiento_p1, golpe_p1, player2_energia)
    print(d['mensaje'])
    return d
    
def player2(turno,movimiento_p2,golpe_p2,player1_energia):
    d = dict();
    energia_quita = golpe_especial("Arnaldor Shuatseneguer", movimiento_p2, golpe_p2)
    #print('player 2 quita', energia_quita)
    d['player1_energia'] = calcular_energia_restante(player1_energia, energia_quita)
    d['mensaje'] = narrar_turno(turno, "Arnaldor Shuatseneguer", movimiento_p2, golpe_p2, player1_energia)
    print(d['mensaje'])
    return d


def narrar_turno(turno, jugador, movimiento, golpe, energia_restante):
    golpe_actual = movimiento + " + " + golpe if golpe else movimiento
    mensaje = f"Turno {turno}: {jugador} ataca con {golpe_actual}"
    return mensaje

def simular_pelea(json_pelea):
    player1_energia = calcular_energia_inicial()
    player2_energia = calcular_energia_inicial()
    turno = 1
    
    movimiento_p1_total = json_pelea["player1"]["movimientos"]
    golpe_p1_total = json_pelea["player1"]["golpes"]
    movimiento_p2_total = json_pelea["player2"]["movimientos"]
    golpe_p2_total = json_pelea["player2"]["golpes"]
    
    
    player1_golpea = len(movimiento_p1_total + golpe_p1_total) < len(movimiento_p2_total + golpe_p2_total)
        #caso de empate
    if len(movimiento_p1_total + golpe_p1_total) == len(movimiento_p2_total + golpe_p2_total):
        player1_golpea = len(movimiento_p1_total) < len(movimiento_p2_total)
        if len(movimiento_p1_total) == len(movimiento_p2_total):
            player1_golpea = True
            
    
    if player1_golpea:
        player = "player1"
    else:
        player = "player2"

    for i in range(len(json_pelea[player]["movimientos"])):
        movimiento_p1 = json_pelea["player1"]["movimientos"][i]
        golpe_p1 = json_pelea["player1"]["golpes"][i]
        movimiento_p2 = json_pelea["player2"]["movimientos"][i]
        golpe_p2 = json_pelea["player2"]["golpes"][i]

        if player1_golpea:
            res1 = player1(turno,movimiento_p1,golpe_p1,player2_energia)
            res2 = player2(turno,movimiento_p2,golpe_p2,player1_energia)
        else:
            res2 = player2(turno,movimiento_p2,golpe_p2,player1_energia)
            res1 = player1(turno,movimiento_p1,golpe_p1,player2_energia)

        turno += 1
        player1_energia = res2['player1_energia']
        player2_energia = res1['player2_energia']

        if res1['player2_energia'] <= 0 or res2['player1_energia'] <= 0:
            break

    ganador = determinar_ganador(turno - 1, player1_energia, player2_energia)
    mensaje_final = f"\n¡La pelea ha terminado! Ganador: {ganador}"
    print(mensaje_final)


json_pelea = {"player1":{"movimientos":["DSD", "S"] ,"golpes":[ "P", ""]}, 
"player2":{"movimientos":["", "ASA", "DA", "AAA", "", "SA"],"golpes":["P", "", "P", "K", "K", "K"]}}

simular_pelea(json_pelea)
